#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  // 도시 목록
  const char *cities[] = {"서울", "아디스아바바", "탈린", "프라하", "도지와 함께 화성", "대전", "도쿄", "평양", "모스크바", "베를린","파리"};

  // 도시를 선택합니다.
  srand(time(NULL));
  int city_index = rand() % (sizeof(cities) / sizeof(cities[0]));

  // 결과를 인쇄합니다.
  printf("선우킹께서 GPS 축지법으로 %s(으)로 가셨습네다.\n", cities[city_index]);

  return 0;
}
